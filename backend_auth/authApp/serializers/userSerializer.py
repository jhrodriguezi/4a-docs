from authApp.models.user import User
from rest_framework      import serializers

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'name', 'lastname', 'username', 'password', 'email', 'address']

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        return {
            'id'        : user.id,
            'name'      : user.name,
            'lastname'  : user.lastname,
            'username'  : user.username,
            'email'     : user.email,
            'address'   : user.address,
        }