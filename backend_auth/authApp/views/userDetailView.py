from django.db.models import query
from rest_framework import status, generics
from django.conf import settings

from authApp.serializers.userSerializer import UserSerializer
from authApp.models.user import User

class UserDetailView(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    def get(self, request, *args, **kwargs):
        return super().get(self, request, *args, **kwargs)

class UserUpdateView(generics.UpdateAPIView):
    queryset         = User.objects.all()
    serializer_class = UserSerializer
    def update(self, request, *args, **kwargs):
        return super().update(self, request, *args, **kwargs)

class UserDeleteView(generics.DestroyAPIView):
    queryset         = User.objects.all()
    serializer_class = UserSerializer
    def delete(self, request, *args, **kwargs):
        return super().destroy(self, request, *args, **kwargs)
